package com.news.news.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dossy on 6/3/2018.
 */

public class PrefManager {

    public static final String NAMA_APLIKASI="news";
    public static final String CEK_LOGIN="cekLogin";
    public static final String USERNAME="username";
    public static final String PASSWORD="password";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public PrefManager(Context context) {
            sharedPreferences=context.
                    getSharedPreferences(NAMA_APLIKASI,Context.MODE_PRIVATE);
            editor=sharedPreferences.edit();
    }

    public void setCekLogin(String key,boolean value){
        editor.putBoolean(key,value);
        editor.commit();
    }

    public void setUsername(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }

    public void setPassword(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }

    public Boolean getCekLogin() {
        return sharedPreferences.getBoolean(CEK_LOGIN,false);
    }

    public String getUsername() {
        return sharedPreferences.getString(USERNAME,"admin");
    }

    public String getPassword() {
        return sharedPreferences.getString(PASSWORD,"1234");
    }

}