package com.news.news;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.news.news.responses.ApiEndPoint;
import com.news.news.responses.RegisterResponse;
import com.news.news.services.ApiClient;
import com.news.news.utils.PopupUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.et_nama)
    EditText et_nama;
    @BindView(R.id.et_username)
    EditText et_username;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.sp_role)
    Spinner sp_role;
    @BindView(R.id.btn_register)
    Button btn_register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=et_nama.getText().toString();
                String username=et_username.getText().toString();
                String password=et_password.getText().toString();
                String role=sp_role.getSelectedItem().toString();

                register(name,username,password,role);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int menuId = item.getItemId();

        if (menuId == android.R.id.home) {
            //Do Something
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void register(String name, String username,String password, String role){

        PopupUtil.showLoading(this,"Register In..","Please Wait ..");

        ApiEndPoint apiEndPoint= ApiClient.getClient().create(ApiEndPoint.class);
        Call<RegisterResponse> call=apiEndPoint.register(name,username,password,role);

        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                PopupUtil.dismissDialog();

                Toast.makeText(getApplicationContext(), "Berhasil Daftar", Toast.LENGTH_LONG).show();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                PopupUtil.dismissDialog();
            }
        });
    }
}