package com.news.news;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.news.news.responses.ApiEndPoint;
import com.news.news.responses.LoginResponse;
import com.news.news.services.ApiClient;
import com.news.news.utils.PopupUtil;
import com.news.news.utils.PrefManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.et_username)
    EditText et_username;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.cb_remember)
    CheckBox cb_remember;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.btn_register)
    Button btn_register;

    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefManager=new PrefManager(this);

        ButterKnife.bind(this);

        if (prefManager.getCekLogin()){
            et_username.setText(prefManager.getUsername());
            et_password.setText(prefManager.getPassword());

        }
        //System.out.println(" Username : "+prefManager.getUsername()+"Password: "+prefManager.getPassword());
        /*
        if (prefManager.getUsername().equals("")&&prefManager.getPassword().equals("")){
            et_password.setText(prefManager.getUsername());
            et_username.setText(prefManager.getPassword());
        }*/


    }


    @OnClick({R.id.btn_register,R.id.btn_login})
    public void onClick(View view){
        int getId=view.getId();
        switch (getId){

            case R.id.btn_login:

                String username=et_username.getText().toString();
                String password=et_password.getText().toString();

                if (cb_remember.isChecked()){
                    prefManager.setUsername(PrefManager.USERNAME,username);
                    prefManager.setPassword(PrefManager.PASSWORD,password);
                }

                login(username,password);
                break;
            case R.id.btn_register:
                startActivity(new Intent(this,RegisterActivity.class));
                break;

        }
    }

    public void login(final String Username, String Password){

        PopupUtil.showLoading(this,"Logging In..","Please Wait ..");

        ApiEndPoint apiEndPoint= ApiClient.getClient().create(ApiEndPoint.class);
        Call<LoginResponse> call=apiEndPoint.loginRequest(Username,Password);


        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                PopupUtil.dismissDialog();

                LoginResponse loginResponse=response.body();

                if (loginResponse!=null){
                    Log.d("Response Data ","Total Data"+loginResponse.getStatus());
                    if(loginResponse.getStatus()){

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                prefManager.setCekLogin(PrefManager.CEK_LOGIN,true);

                                startActivity(new Intent(getApplicationContext(), SortCheckActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                finish();
                            }
                        });

                    }else{
                        Toast.makeText(getApplicationContext(), "Email Atau Password Salah", Toast.LENGTH_SHORT).show();
                    }


                }else{
                    Log.d("Login : ","Data Null");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                PopupUtil.dismissDialog();
                Toast.makeText(getApplicationContext(), "Email Atau Password Salah", Toast.LENGTH_SHORT).show();

            }
        });
    }

}