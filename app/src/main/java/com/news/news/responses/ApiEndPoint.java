package com.news.news.responses;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Dossy on 5/21/2018.
 */

public interface ApiEndPoint {

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> loginRequest(@Field("username") String username,
                                     @Field("password") String password);

    @FormUrlEncoded
    @POST("register.php")
    Call<RegisterResponse> register(@Field("name") String name,
                                    @Field("username") String username,
                                    @Field("password") String password,
                                    @Field("role") String role);

    /*
    @GET("get_store.php")
    //Call<StoreResponse> getStore(@Query("lat") double lat,@Query("lng") double lng);
    Call<StoreResponse> getStore();

    @GET("get_product.php")
    Call<ProductResponse> getProduct(@Query("store_id") String storeId);

    @GET("get_product_detail.php")
    Call<ProductDetailResponse> getProductDetail(@Query("product_id") String productId);

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> login(@Field("Username") String username, @Field("Password") String password);

    @FormUrlEncoded
    @POST("checkout.php")
    Call<CheckoutResponse> checkout(@Field("name") String Name,
                                    @Field("no_hp") String No_Hp,
                                    @Field("address") String Address,
                                    @Field("productIds") String ProductIds,
                                    @Field("prices") String Prices);
    */
}