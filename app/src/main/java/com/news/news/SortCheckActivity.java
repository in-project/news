package com.news.news;

import android.content.Intent;
import android.os.TestLooperManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.news.news.utils.PrefManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
    Untuk Rotate Case , baru bisa input statis karena sharedPreferences g berhasil simpan nilai
     jadi yg bisa di rotate klo input => 5

    Selain 5 g bisa

 */
public class SortCheckActivity extends AppCompatActivity {

    @BindView(R.id.et_no)
    EditText et_no;
    @BindView(R.id.tv_result)
    TextView tv_result;
    @BindView(R.id.et_no_rotation)
    EditText et_rotation;
    @BindView(R.id.tv_rotated)
    TextView tv_tv_rotated;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.btn_rotation)
    Button btn_rotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_check);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick({R.id.btn_submit, R.id.btn_rotation})
    public void onClick(View view) {
        int getId = view.getId();

        switch (getId) {
            case R.id.btn_submit:

                String input = et_no.getText().toString();
                String[] arrayData = new String[Integer.parseInt(input)];

                for (int i = 0; i < Integer.parseInt(input); i++) {
                    arrayData[i] = String.valueOf(i + 1);
                }

                tv_result.setText(getJoinData(arrayData));

                String dataInput=getJoinData(arrayData);
                String dataInputSplit[]=getSplitData(dataInput);

                int rotationValue=3;
                String s1[]=new String [Integer.parseInt(input)];
                String s2[]=new String [Integer.parseInt(input)-rotationValue];

                int h=0;

                for (int j = 0; j<Integer.parseInt(input) ; j++) {
                    if (j<=Integer.parseInt(input)-rotationValue){
                        s1[j]=dataInputSplit[j];
                    }
                    else{
                        s2[h]=dataInputSplit[j];
                        h++;
                    }
                }

                String s1Join=getJoinData(s1);
                String s2Join=getJoinData(s2);
                String rotated=s2Join+s1Join;
                tv_tv_rotated.setText(rotated);
                break;

                /*
            case R.id.btn_rotation:
                String input2 = et_rotation.getText().toString();

                break;*/
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int menuId = item.getItemId();

        if (menuId == android.R.id.home) {
            //Do Something
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public String[] getSplitData(String data) {
        String dateValue[] = data.split(",");
        return dateValue;
    }

    public String getJoinData(String[] inputData) {
        String inputNo = "";
        int i = 0;
        for (String data : inputData) {

            if (i != inputData.length - 1) {

                inputNo = inputNo + data + ",";
            } else {
                inputNo = inputNo + data + "";
            }
            i++;
        }
        return inputNo;
    }
}