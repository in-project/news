package com.news.news.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dossy on 5/21/2018.
 */

public class ApiClient {


    public static final String BASE_URL="http://192.168.43.170/News/";
    //public static final String BASE_URL_NEWS="https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=48be605498584d4793bd63e51292c65e";

    private static Retrofit retrofit=null;


    public static Retrofit getClient(){

        if (retrofit==null){
            retrofit=new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }


}
